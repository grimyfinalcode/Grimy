import SpriteKit

enum Direction {
    case LEFT
    case RIGHT
}

class GameScene: SKScene, SKPhysicsContactDelegate{
    
    //início da implementacao da colisao
    //estrutura de físicas
    struct PhysicsCategory {
        
        static let None: UInt32 = 1
        static let Square: UInt32 = 0b1
        static let Ball: UInt32 = 0b10
        static let Border: UInt32 = 0b100
        static let All: UInt32 = UInt32.max
        
    }
    
    //primeira deteccao de contato
    func didBeginContact(contact: SKPhysicsContact){
        var corpo1: SKPhysicsBody
        var corpo2: SKPhysicsBody
        
        corpo1 = contact.bodyA
        corpo2 = contact.bodyB
        
        if ((corpo1.node?.name == "grimy") && (corpo2.node?.name == "soap")){
            corpo2.node?.removeFromParent()
            pontuacao = pontuacao + 1
            print("\(pontuacao)")
        }
    }
    
    var grimy: SKSpriteNode?
    var leftArrow: SKSpriteNode?
    var rightArrow: SKSpriteNode?
    var buttonA: SKSpriteNode?
    var buttonB: SKSpriteNode?
    
    var soap: SKSpriteNode?
    
    var buttonsCreated = [SKSpriteNode]()
    var pressedButtons = [SKSpriteNode]()
    
    var speedT: CGFloat = 15.0
    var direction: Direction = Direction.RIGHT
    
    //pontuacao
    var pontuacao = 1
    var labelPontuacao : SKLabelNode?
    
    var isJumping = false
    
    override func didMoveToView(view: SKView){
        
        grimy = self.childNodeWithName("grimy") as? SKSpriteNode

        //Contato *******
        
        self.physicsWorld.gravity = CGVectorMake(0, -9.8)
        physicsWorld.contactDelegate = self
        let sceneBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        sceneBody.friction = 0
        self.physicsBody = sceneBody
        
        //Fim Contato *********
        
        //adicionando o contador como filho da cena
        /* Setup your scene here */
//        let myLabel = SKLabelNode(fontNamed:"Arial")
//        myLabel.text = "\(pontuacao)"
//        myLabel.fontSize = 32
//        myLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame))
//        myLabel.zPosition = 200
//        
//        camera?.addChild(myLabel)
        
        labelPontuacao = camera?.childNodeWithName("labelPontuacao") as? SKLabelNode
        
        
        //adicionando objeto como filho da cena
        
        
        //criando o sabonete na cena
        //troquei para camera
        soap = self.childNodeWithName("soap") as? SKSpriteNode
        //soap = camera?.childNodeWithName("soap") as? SKSpriteNode
        
        //criando acoes e sequencia para o sabonete puslar
        
        
        let zoomIn = SKAction.scaleTo(1.6, duration: 0.5)
        let zoomOut = SKAction.scaleTo(1.0, duration: 0.5)
        let seqZoom = SKAction.sequence([zoomIn, zoomOut])
        let repeater = SKAction.repeatActionForever(seqZoom)
        soap?.runAction(repeater) //repete acoes de zoom do sabonete
        
        
        self.physicsBody?.categoryBitMask = PhysicsCategory.Border
        self.physicsBody?.collisionBitMask = PhysicsCategory.Ball
        self.physicsBody?.contactTestBitMask = PhysicsCategory.None

        //adicionando física no grimy
        grimy!.physicsBody?.categoryBitMask = PhysicsCategory.Square
        grimy!.physicsBody?.collisionBitMask = PhysicsCategory.None
        grimy!.physicsBody?.contactTestBitMask = PhysicsCategory.Ball
        //adicionando física no soap
        
        soap!.physicsBody?.categoryBitMask = PhysicsCategory.Ball
        soap!.physicsBody?.collisionBitMask = PhysicsCategory.Border | PhysicsCategory.Ball
        soap!.physicsBody?.contactTestBitMask = PhysicsCategory.Square
        
        
        createCamera()
        
        if(camera != nil){
            leftArrow = camera!.childNodeWithName("leftArrow") as? SKSpriteNode
            buttonsCreated.append(leftArrow!)
            
            rightArrow = camera!.childNodeWithName("rightArrow") as? SKSpriteNode
            buttonsCreated.append(rightArrow!)
            
            buttonA = camera!.childNodeWithName("buttonA") as? SKSpriteNode
            buttonsCreated.append(buttonA!)

        }
        
    }
    
    override func update(currentTime: CFTimeInterval) {
        if ((pressedButtons.indexOf(buttonA!) != nil)&&(grimy?.physicsBody?.velocity.dy == 0)) {
            if pressedButtons.indexOf(leftArrow!) != nil {
                jump(Direction.LEFT)
            } else if pressedButtons.indexOf(rightArrow!) != nil {
                jump(Direction.RIGHT)
            } else {
                simpleJump()
            }
        }
        
        if pressedButtons.indexOf(leftArrow!) != nil {
            walk(Direction.LEFT)
        }
        if pressedButtons.indexOf(rightArrow!) != nil {
            walk(Direction.RIGHT)
        }
        
        labelPontuacao?.text = "\(pontuacao)"
        createCamera()
    }
    
    //crei agora a câmera -> Estou testando
    func createCamera(){
        if let camera = camera  {
            //SKScene_0
            //minha configuracao
            //camera.position = CGPoint(x: grimy!.position.x, y: 768) CÓDIGO CORRETO
            
            //cs rio código
            //let posicaoy = (self.childNodeWithName("noReference") as! SKSpriteNode).position.y
            camera.position = CGPoint(x: 280 + grimy!.position.x , y: 768)
            
        }
    }
    //815,055
    //365,701
    func walk(direction: Direction) {
        print(#function)
        if direction == Direction.RIGHT {
            grimy!.position = CGPoint(x: (grimy!.position.x) + speedT,
                                        y: grimy!.position.y)
            //muda de lado right
            grimy?.xScale = 1
        } else {
            grimy!.position = CGPoint(x: (grimy!.position.x) - speedT,
                                        y: grimy!.position.y)
            //muda de lado esquerda
            grimy?.xScale = -1
        }
    }
    
    func simpleJump() {
        grimy!.physicsBody?.applyImpulse(CGVectorMake(0, 800))
        //grimy?.zPosition = 5
    }
    
    func jump(direction: Direction) {
        if (direction == Direction.LEFT) {
            grimy!.physicsBody?.applyImpulse(CGVectorMake(-100, 800))
        } else {
            grimy!.physicsBody?.applyImpulse(CGVectorMake(100, 800))
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.locationInNode(camera!)
            for button in buttonsCreated {
                if button.containsPoint(location) && pressedButtons.indexOf(button) == nil {
                    pressedButtons.append(button)
                }
            }
        }
        
        for button in buttonsCreated {
            if pressedButtons.indexOf(button) == nil {
                button.alpha = 0.2
            } else {
                button.alpha = 0.8
            }
        }
    }
    
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.locationInNode(camera!)
            let previousLocation = touch.previousLocationInNode(camera!)
            
            for button in buttonsCreated {
                if button.containsPoint(previousLocation) && !button.containsPoint(location) {
                    let index = pressedButtons.indexOf(button)
                    if index != nil {
                        pressedButtons.removeAtIndex(index!)
                    }
                } else if !button.containsPoint(previousLocation) && button.containsPoint(location) && pressedButtons.indexOf(button) == nil {
                    pressedButtons.append(button)
                }
            }
        }
        
        for button in buttonsCreated {
            if pressedButtons.indexOf(button) == nil {
                button.alpha = 0.2
            } else {
                button.alpha = 0.8
            }
        }
    }
    
    func touchsEndOrCancelled(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.locationInNode(camera!)
            let previousLocation = touch.previousLocationInNode(camera!)
            
            for button in buttonsCreated {
                if button.containsPoint(location) {
                    let index = pressedButtons.indexOf(button)
                    if index != nil {
                        pressedButtons.removeAtIndex(index!)
                    }
                }
                else if (button.containsPoint(previousLocation)) {
                    let index = pressedButtons.indexOf(button)
                    if index != nil {
                        pressedButtons.removeAtIndex(index!)
                    }
                }
            }
        }
        
        for button in buttonsCreated {
            if pressedButtons.indexOf(button) == nil {
                button.alpha = 0.2
            }
            else {
                button.alpha = 0.8
            }
        }
    }
    
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        touchsEndOrCancelled(touches, withEvent: event)
    }
    
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        touchsEndOrCancelled(touches!, withEvent: event)
    }
    
}
