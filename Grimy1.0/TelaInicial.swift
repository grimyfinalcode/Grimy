//
//  TelaInicial.swift
//  Grimy1.0
//
//  Created by Jerlilson Bezerra da Silva on 19/05/16.
//  Copyright © 2016 Jerlilson Bezerra da Silva. All rights reserved.
//

import UIKit
import SpriteKit

class TelaInicial: SKScene {
    var play : SKSpriteNode?
    
    override func didMoveToView(view: SKView) {
        play = self.childNodeWithName("play") as? SKSpriteNode
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches{
            let positionToque = touch.locationInNode(self)
            let guardaToque = self.nodeAtPoint(positionToque)
            
            //if(guardaToque != nil){
                if (guardaToque.name == "play"){
                    let transicao = SKTransition.doorwayWithDuration(1.0)
                    let recebeCena = GameScene(fileNamed: "GameScene")
                    //recebeCena?.scaleMode = scaleMode
                    recebeCena!.scaleMode = .AspectFill
                    self.view?.presentScene(recebeCena!, transition: transicao)
                }
            //}
            
        }
    }
    
    
    
}
